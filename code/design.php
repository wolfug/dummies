<?php
namespace Design;

interface GoAlgorithm {
	public function go();
}


class GoByDrivingAlgorithm implements GoAlgorithm {
	public function go()
	{
		return "I'm driving";
	}
}

class GoByFlyAlgorithm implements GoAlgorithm {
	public function go()
	{
		return "I'm flying.";
	}
}

class GoByFlyFast implements GoAlgorithm {
	public function go()
	{
		return "Now I'm flying fast.";
	}
}


class Vehicle {
	protected $goAlgorithm;

	public function setGoAlgorithm(GoAlgorithm $goAlgorithm) {
		$this->goAlgorithm = $goAlgorithm;
	}
	public function go() {

		return $this->goAlgorithm->go();
	}
}


class StreetRaced extends Vehicle {

}

class FormulaOne extends Vehicle {
	
}


class Helicopter extends Vehicle
{
	
	
}

class Jet extends Vehicle
{
	
}

$streetRaced = new StreetRaced();
$streetRaced->setGoAlgorithm(new GoByFlyAlgorithm());
printResult($streetRaced->go());


 $formulaOne = new FormulaOne();
 $formulaOne->setGoAlgorithm(new GoByDrivingAlgorithm());
 printResult($formulaOne->go());


 $helicopter = new Helicopter();
 $helicopter->setGoAlgorithm(new GoByFlyAlgorithm());
 printResult($helicopter->go());

 $jet = new Jet();
 $jet->setGoAlgorithm(new GoByFlyAlgorithm());
printResult($jet->go());

 $jet->setGoAlgorithm(new GoByFlyFast());
 printResult($jet->go());


function printResult($data)
{
	echo "<h1>{$data}</h1><br>";
}
